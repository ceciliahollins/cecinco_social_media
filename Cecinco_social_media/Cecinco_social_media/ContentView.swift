//
//  ContentView.swift
//  Cecinco_social_media
//
//  Created by Cecilia Hollins on 14/2/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
