//
//  Cecinco_social_mediaApp.swift
//  Cecinco_social_media
//
//  Created by Cecilia Hollins on 14/2/22.
//

import SwiftUI

@main
struct Cecinco_social_mediaApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
