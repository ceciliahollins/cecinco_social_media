//
//  ColorScheme.swift
//  Cecinco_social_media
//
//  Created by Cecilia Hollins on 15/2/22.
//

import Foundation
import SwiftUI

public struct ColorScheme {
    
    public static var primary: Color {
        return Color("mediumBlue")
    }
    
    public static var secondary: Color {
        return Color("orange")
    }
    
    public static var neutralLight: Color {
        return Color("lightBlue")
    }
    
    public static var neutralMedium: Color {
        return Color("darkBlue")
    }
    
    public static var neutralDark: Color {
        return Color("darkNeutral")
    }
    
}
