//
//  FontScheme.swift
//  Cecinco_social_media
//
//  Created by Cecilia Hollins on 15/2/22.
//

import Foundation
import SwiftUI

public struct FontScheme {
    
    public static var h1: Font {
        return Font.custom("Propaganda", size: 35)
    }
    
    public static var h2: Font {
        return Font.custom("Propaganda", size: 30)
    }
    
    public static var body: Font {
        return Font.custom("Propaganda", size: 20)
    }
    
    public static var caption: Font {
        return Font.custom("Propaganda", size: 15)
    }
}
