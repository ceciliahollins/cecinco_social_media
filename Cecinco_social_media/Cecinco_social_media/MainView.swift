//
//  MainView.swift
//  Cecinco_social_media
//
//  Created by Franco Buena on 16/2/22.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            ContentView()
            .tabItem {
                Label("Home", systemImage: "house")
            }
            Text("Tab Content 2") // replace with appropriate view
                .tabItem {
                Label("Discover", systemImage: "magnifyingglass")
            }
            Text("Tab Content 3") // replace with appropriate view
                .tabItem {
                Label("Profile", systemImage: "person")
            }
            Text("Tab Content 4") // replace with appropriate view
                .tabItem {
                Label("Settings", systemImage: "gear")
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
